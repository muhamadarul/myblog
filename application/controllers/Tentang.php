<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tentang extends CI_Controller
{
    public function __construct()
    {
      parent::__construct();
      $this->load->helper('url');
      $this->load->model('Logo_model');
      $this->load->model('Tentang_model');

  }

  public function index()
  {
      $data['logo'] = $this->Logo_model->viewLogo();
      $data['view'] = $this->Tentang_model->view();
      $data['title'] = 'MYBLOG | Tentang';
      // echo 'Selamat datang ' . $data['user']['nama'];
      $this->load->view('templates/front_header', $data);
      $this->load->view('front/tentang/index', $data);
      $this->load->view('templates/front_footer');

    }
}
