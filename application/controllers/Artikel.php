<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Artikel extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Logo_model');
        $this->load->model('Artikel_model');
        $this->load->model('kategori_artikel_model');
        $this->load->library('pagination');
        $this->load->model('Komentar_model');
    }

    public function index()
    {
      //konfigurasi pagination
      $config['base_url'] = base_url('artikel/index'); //site url
      $config['total_rows'] = $this->db->count_all('artikel'); //total row
      $config['per_page'] = 2;  //show record per halaman
      $config["uri_segment"] = 3;  // uri parameter
      $choice = $config["total_rows"] / $config["per_page"];
      $config["num_links"] = floor($choice);

      // Membuat Style pagination untuk BootStrap v4
      $config['first_link']       = 'First';
      $config['last_link']        = 'Last';
      $config['next_link']        = 'Next';
      $config['prev_link']        = 'Prev';
      $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
      $config['full_tag_close']   = '</ul></nav></div>';
      $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
      $config['num_tag_close']    = '</span></li>';
      $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
      $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
      $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
      $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
      $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
      $config['prev_tagl_close']  = '</span>Next</li>';
      $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
      $config['first_tagl_close'] = '</span></li>';
      $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
      $config['last_tagl_close']  = '</span></li>';

      $this->pagination->initialize($config);
      $data['page'] = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

      //panggil function get_mahasiswa_list yang ada pada mmodel mahasiswa_model.
      $data['artikel'] = $this->Artikel_model->get_artikel_list($config["per_page"], $data['page']);

      $data['pagination'] = $this->pagination->create_links();

        $data['logo'] = $this->Logo_model->viewLogo();
        $data['title'] = 'MYBLOG | Artikel';
        $this->load->view('templates/front_header', $data);
        $this->load->view('front/artikel/index', $data);
        $this->load->view('templates/front_footer');
    }

    public function detail($id)
    {
        $data['logo'] = $this->Logo_model->viewLogo();
        $data['artikel'] = $this->Artikel_model->getDetail($id);
        $data['kategori'] = $this->kategori_artikel_model->view();
        $data['komentar'] = $this->Komentar_model->viewByArtikel($id);
        $data['title'] = 'MYBLOG | Artikel';
        $this->load->view('templates/front_header', $data);
        $this->load->view('front/artikel/detail', $data);
        $this->load->view('templates/front_footer');
    }
    public function viewByKategori()
    {
      $data['logo'] = $this->Logo_model->viewLogo();
      $data['artikel'] = $this->Artikel_model->getDetail($id);
    }
    public function saveKomentar()
    {
      $postingan = $this->input->post('postingan');
      $this->form_validation->set_rules('komentar', 'komentar', 'required|trim', [
          'required' => 'komentar tidak boleh kosong!'
      ]);
      $this->form_validation->set_rules('email', 'email', 'required|trim', [
          'required' => 'email tidak boleh kosong!'
      ]);
      if ($this->form_validation->run() == false) {
          redirect('artikel/detail/'.$postingan);
      } else {
          $this->Komentar_model->tambah();
          $this->session->set_flashdata('success', 'komentar berhasil ditambahkan!');
          redirect('artikel/detail/'.$postingan);
      }
    }
}
