<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('Auth_model');
    $this->load->helper('url');
	}

	public function index()
	{
		$this->form_validation->set_rules('username', 'Username', 'required', [
			'required' => 'Username tidak boleh kosong!'
		]);
		$this->form_validation->set_rules('password', 'Password', 'required|trim', [
			'required' => 'Password tidak boleh kosong'
		]);

		if ($this->form_validation->run() == false) {
			$data['title'] = 'Login Pengguna';

			$this->load->view('templates/auth_header', $data);
			$this->load->view('auth/login');
			$this->load->view('templates/auth_footer');
		} else {
			$username = $this->input->post('username');
			$password = $this->input->post('password');

			$user = $this->Auth_model->login($username, $password);

			if ($user) {
				//jika usernya sudah aktif
				if ($user['aktif'] == 1) {
					//cek password
					if (password_verify($password, $user['password'])) {
						$data = [
							'username' => $user['username'],
							'id_akses' => $user['id_akses']
						];
						$this->session->set_userdata($data);
						redirect('admin/beranda');
					} else {
						$this->session->set_flashdata('warning', 'Password salah, periksa kembali');
						redirect('auth');
					}
				} else {
					$this->session->set_flashdata('warning', 'Username belum diaktivasi');
					redirect('auth');
				}
			} else {
				$this->session->set_flashdata('error', 'Username belum terdaftar!');
				redirect('auth');
			}
		}
	}

	public function lupa_password()
	{
		$data['title'] = 'Lupa Password';
		$this->load->view('templates/auth_header', $data);
		$this->load->view('auth/lupa_password');
		$this->load->view('templates/auth_footer');
	}

	public function logout()
	{
		
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('id_akses');
		$this->session->set_flashdata('success', 'Anda telah logout');
		redirect('auth');
	}

}
