<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Galeri extends CI_Controller
{
    public function __construct()
    {
      parent::__construct();
      $this->load->helper('url');
      $this->load->model('Logo_model');
      $this->load->model('Galeri_model');
  }

  public function index()
  {
      $data['logo'] = $this->Logo_model->viewLogo();
      $data['galeri'] = $this->Galeri_model->view();
      $data['title'] = 'MYBLOG | Galeri';
      // echo 'Selamat datang ' . $data['user']['nama'];
      $this->load->view('templates/front_header', $data);
      $this->load->view('front/galeri/index', $data);
      $this->load->view('templates/front_footer');

    }
}
