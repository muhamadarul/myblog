<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Beranda extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Slider_model');
        $this->load->model('Logo_model');
        $this->load->model('Artikel_model');
        $this->load->model('Tentang_model');
        $this->load->model('Galeri_model');
    }

    public function index()
    {
        $data['slider'] = $this->Slider_model->viewSlider();
        $data['logo'] = $this->Logo_model->viewLogo();
        $data['artikel'] = $this->Artikel_model->viewWithLimit();
        $data['tentang'] = $this->Tentang_model->view();
        $data['galeri'] = $this->Galeri_model->viewLimit();
        $data['title'] = 'MYBLOG | Beranda';
        $this->load->view('templates/front_header', $data);
        $this->load->view('front/beranda/index', $data);
        $this->load->view('templates/front_footer');
    }

}
