<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tentang extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Auth_model');
        $this->load->helper('url');
        $this->load->model('Tentang_model');
        if (!$this->session->userdata('username')) {
                $this->session->set_flashdata('error', 'Anda belum melakukan login!');
                redirect('auth');
            }
    }

    public function index()
    {
        $data['user'] = $this->Auth_model->success_login();
        $data['view'] = $this->Tentang_model->view();
        $data['title'] = 'Admin | Tentang';
        // echo 'Selamat datang ' . $data['user']['nama'];
        $this->load->view('templates/header', $data);
        $this->load->view('admin/tentang/index', $data);
        $this->load->view('templates/footer');
    }
    public function ubah($id = null)
    {
        $data['user'] = $this->Auth_model->success_login();
        $data['view'] = $this->Tentang_model->getById($id);
        $data['title'] = 'Admin | Ubah Tentang';
        $this->form_validation->set_rules('nama', 'nama', 'required|trim', [
            'required' => 'nama tidak boleh kosong!'
        ]);
        $this->form_validation->set_rules('deskripsi', 'deskripsi', 'required|trim', [
            'required' => 'deskripsi tidak boleh kosong!'
        ]);
        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('admin/tentang/ubah', $data);
            $this->load->view('templates/footer');
        } else {
            $this->Tentang_model->ubah($id);
            $this->session->set_flashdata('success', 'Data berhasil diubah!');
            redirect('admin/tentang');
        }
    }
}
