<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Beranda extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Auth_model');
        $this->load->helper('url');
        $this->load->model('Beranda_model');
        if (!$this->session->userdata('username')) {
                $this->session->set_flashdata('error', 'Anda belum melakukan login!');
                redirect('auth');
            }
    }

    public function index()
    {
        $data['user'] = $this->Auth_model->success_login();
        $data['hitunguser'] = $this->Beranda_model->hitunguser();
        $data['title'] = 'Admin | Dashboard';
        // echo 'Selamat datang ' . $data['user']['nama'];
        $this->load->view('templates/header', $data);
        $this->load->view('admin/beranda/index', $data);
        $this->load->view('templates/footer');
    }
}
