<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Komentar extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Auth_model');
        $this->load->model('Komentar_model');
        $this->load->helper(array('url','form'));
        if (!$this->session->userdata('username')) {
                $this->session->set_flashdata('error', 'Anda belum melakukan login!');
                redirect('auth');
            }

        if ($this->session->userdata('id_akses') == 2) {
            redirect('admin/blank');
        }
    }

    public function index()
    {
        $data['user'] = $this->Auth_model->success_login();
        $data['title'] = 'Admin | Komentar';
        $data['view'] = $this->Komentar_model->view();
        // echo 'Selamat datang ' . $data['user']['nama'];
        $this->load->view('templates/header', $data);
        $this->load->view('admin/komentar/index', $data);
        $this->load->view('templates/footer');
    }

    public function hapus($id)
    {
        $this->Komentar_model->hapus($id);
        $this->session->set_flashdata('success', 'Data berhasil dihapus');
        redirect('admin/komentar');
    }

}
