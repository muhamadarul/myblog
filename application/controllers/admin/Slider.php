<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Slider extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Auth_model');
        $this->load->model('Slider_model');
        $this->load->helper(array('url','form'));
        if (!$this->session->userdata('username')) {
                $this->session->set_flashdata('error', 'Anda belum melakukan login!');
                redirect('auth');
            }

        if ($this->session->userdata('id_akses') == 2) {
            redirect('admin/blank');
        }
    }

    public function index()
    {
        $data['user'] = $this->Auth_model->success_login();
        $data['title'] = 'Admin | Slider';
        $data['view'] = $this->Slider_model->view();
        // echo 'Selamat datang ' . $data['user']['nama'];
        $this->load->view('templates/header', $data);
        $this->load->view('admin/slider/index', $data);
        $this->load->view('templates/footer');
    }

    public function tambah()
    {
      $data['user'] = $this->Auth_model->success_login();
      $data['title'] = 'Admin | Tambah Slide';

      $this->form_validation->set_rules('gambar', 'gambar', [
          'required' => 'Gambar tidak boleh kosong!'
      ]);

      $this->form_validation->set_rules('status', 'status', 'required|trim', [
          'required' => 'status tidak boleh kosong!'

      ]);
      if  ($this->form_validation->run() == false) {
          $this->load->view('templates/header', $data);
          $this->load->view('admin/slider/tambah', $data);
          $this->load->view('templates/footer');
          // $error = array('error' => $this->upload->display_error());
      } else {
          $this->Slider_model->tambah();
          $this->session->set_flashdata('success', 'Data berhasil ditambahkan!');
          redirect('admin/slider');
      }

    }

    public function hapus($id)
    {
        $this->Slider_model->hapus($id);
        $this->session->set_flashdata('success', 'Data berhasil dihapus');
        redirect('admin/slider');
    }

    public function ubah($id = null)
    {
        $data['user'] = $this->Auth_model->success_login();
        $data['slide'] = $this->Slider_model->getById($id);
        $data['title'] = 'Admin | Ubah Slide';

        $this->form_validation->set_rules('gambar', 'gambar', [
            'required' => 'Gambar tidak boleh kosong!'
        ]);

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('admin/slider/ubah', $data);
            $this->load->view('templates/footer');
        } else {
            $this->Slider_model->ubah($id);
            $this->session->set_flashdata('success', 'Data berhasil diubah!');
            redirect('admin/slider');
        }
    }
}
