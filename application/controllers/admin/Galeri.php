<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Galeri extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Auth_model');
        $this->load->helper('url');
        $this->load->model('Galeri_model');
        if (!$this->session->userdata('username')) {
                $this->session->set_flashdata('error', 'Anda belum melakukan login!');
                redirect('auth');
            }
    }

    public function index()
    {
        $data['user'] = $this->Auth_model->success_login();
        $data['view'] = $this->Galeri_model->view();
        $data['title'] = 'Admin | Galeri';
        $this->load->view('templates/header', $data);
        $this->load->view('admin/galeri/index', $data);
        $this->load->view('templates/footer');
    }

    public function tambah()
    {
        $data['user'] = $this->Auth_model->success_login();
        $data['title'] = 'Admin | Tambah Galeri';
        $this->form_validation->set_rules('caption', 'caption', 'required|trim', [
            'required' => 'Caption tidak boleh kosong!'
        ]);

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('admin/galeri/tambah', $data);
            $this->load->view('templates/footer');
        } else {

            $this->Galeri_model->tambah();
            $this->session->set_flashdata('success', 'Data berhasil ditambahkan!');
            redirect('admin/galeri');
        }
      }

      public function ubah($id = null)
      {
          $data['user'] = $this->Auth_model->success_login();
          $data['view'] = $this->Galeri_model->getById($id);
          $data['title'] = 'Admin | Ubah Galeri';
          $this->form_validation->set_rules('caption', 'caption', 'required|trim', [
              'required' => 'Caption tidak boleh kosong!'
          ]);

          if ($this->form_validation->run() == false) {
              $this->load->view('templates/header', $data);
              $this->load->view('admin/galeri/ubah', $data);
              $this->load->view('templates/footer');
          } else {
              $this->Galeri_model->ubah($id);
              $this->session->set_flashdata('success', 'Data berhasil diubah!');
              redirect('admin/galeri');
          }
      }

      public function hapus($id)
      {
          $this->Galeri_model->hapus($id);
          $this->session->set_flashdata('success', 'Data berhasil dihapus');
          redirect('admin/galeri');
      }
}
