<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Artikel extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Auth_model');
        $this->load->helper('url');
        $this->load->model('Kategori_artikel_model');
        $this->load->model('Artikel_model');
        if (!$this->session->userdata('username')) {
                $this->session->set_flashdata('error', 'Anda belum melakukan login!');
                redirect('auth');
            }
    }

    public function index()
    {
        $data['user'] = $this->Auth_model->success_login();
        $data['view'] = $this->Artikel_model->view();
        $data['title'] = 'Admin | Artikel';
        // echo 'Selamat datang ' . $data['user']['nama'];
        $this->load->view('templates/header', $data);
        $this->load->view('admin/artikel/index', $data);
        $this->load->view('templates/footer');
    }

    public function tambah()
    {
        $data['user'] = $this->Auth_model->success_login();
        $data['kategori'] = $this->Kategori_artikel_model->view();
        $data['title'] = 'Admin | Tambah Artikel';
        $this->form_validation->set_rules('judul', 'Judul', 'required|trim', [
            'required' => 'Judul tidak boleh kosong!'
        ]);
        $this->form_validation->set_rules('isi', 'Isi', 'required|trim', [
            'required' => 'Isi tidak boleh kosong!'
        ]);

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('admin/artikel/tambah', $data);
            $this->load->view('templates/footer');
        } else {

            $this->Artikel_model->tambah();
            $this->session->set_flashdata('success', 'Data berhasil ditambahkan!');
            redirect('admin/artikel');
        }
      }

      public function ubah($id = null)
      {
          $data['user'] = $this->Auth_model->success_login();
          $data['kategori'] = $this->Kategori_artikel_model->view();
          $data['artikel'] = $this->Artikel_model->getById($id);
          $data['title'] = 'Admin | Ubah Artikel';
          $this->form_validation->set_rules('judul', 'Judul', 'required|trim', [
              'required' => 'Judul tidak boleh kosong!'
          ]);
          $this->form_validation->set_rules('isi', 'Isi', 'required|trim', [
              'required' => 'Isi tidak boleh kosong!'
          ]);

          if ($this->form_validation->run() == false) {
              $this->load->view('templates/header', $data);
              $this->load->view('admin/artikel/ubah', $data);
              $this->load->view('templates/footer');
          } else {
              $this->Artikel_model->ubah($id);
              $this->session->set_flashdata('success', 'Data berhasil diubah!');
              redirect('admin/artikel');
          }
      }

      public function hapus($id)
      {
          $this->Artikel_model->hapus($id);
          $this->session->set_flashdata('success', 'Data berhasil dihapus');
          redirect('admin/artikel');
      }
}
