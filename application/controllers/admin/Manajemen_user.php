<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Manajemen_user extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Auth_model');
        $this->load->model('Manajemen_user_model');
        $this->load->helper('url');
        if (!$this->session->userdata('username')) {
                $this->session->set_flashdata('error', 'Anda belum melakukan login!');
                redirect('auth');
            }

        if ($this->session->userdata('id_akses') == 2) {
                redirect('admin/blank');
        }
    }

    public function index()
    {
        $data['user'] = $this->Auth_model->success_login();
        $data['title'] = 'Admin | Manajemen user';
        $data['view'] = $this->Manajemen_user_model->view();
        // echo 'Selamat datang ' . $data['user']['nama'];
        $this->load->view('templates/header', $data);
        $this->load->view('admin/manajemen_user/index', $data);
        $this->load->view('templates/footer');
    }

    public function tambah()
    {
      $data['user'] = $this->Auth_model->success_login();
      $data['akses'] = $this->Manajemen_user_model->aksesview();
      // $data['user'] = $this->Auth_model->success_login();
      $data['title'] = 'Admin | Tambah Admin';

      $this->form_validation->set_rules('nama', 'Nama', 'required|trim', [
          'required' => 'nama tidak boleh kosong!'
      ]);
      $this->form_validation->set_rules('username', 'Username', 'required|trim|is_unique[user.username]', [
          'required' => 'Username tidak boleh kosong!',
          'is_unique' => 'Username sudah digunakan!'
      ]);
      $this->form_validation->set_rules('password1', 'Password', 'required|trim|min_length[6]|matches[password2]', [
          'matches' => 'Password tidak sesuai!',
          'min_length' => 'Password terlalu pendek!',
          'required' => 'Password tidak boleh kosong'
      ]);
      $this->form_validation->set_rules('password2', 'password', 'required|trim|matches[password1]');

      if ($this->form_validation->run() == false) {
          $this->load->view('templates/header', $data);
          $this->load->view('admin/manajemen_user/tambah', $data);
          $this->load->view('templates/footer');

      } else {

          $this->Manajemen_user_model->tambah();
          $this->session->set_flashdata('success', 'Data berhasil ditambahkan!');
          redirect('admin/manajemen_user');
      }
    }

    public function hapus($id)
    {
        $this->Manajemen_user_model->hapus($id);
        $this->session->set_flashdata('success', 'Data berhasil dihapus');
        redirect('admin/manajemen_user');
    }

    public function ubah($id = null)
    {
        $data['akses'] = $this->Manajemen_user_model->aksesview();
        $data['user'] = $this->Auth_model->success_login();
        $data['admin'] = $this->Manajemen_user_model->getById($id);
        $data['title'] = 'Admin | Ubah Admin';

        $this->form_validation->set_rules('nama', 'Nama', 'required|trim', [
            'required' => 'nama tidak boleh kosong!'
        ]);
        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('admin/manajemen_user/ubah', $data);
            $this->load->view('templates/footer');
        } else {
            $this->Manajemen_user_model->ubah();
            $this->session->set_flashdata('success', 'Data berhasil diubah!');
            redirect('admin/manajemen_user');
        }
    }
}
