<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Logo extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Auth_model');
        $this->load->model('Logo_model');
        $this->load->helper(array('url','form'));
        if (!$this->session->userdata('username')) {
                $this->session->set_flashdata('error', 'Anda belum melakukan login!');
                redirect('auth');
            }

        if ($this->session->userdata('id_akses') == 2) {
            redirect('admin/blank');
        }
    }

    public function index()
    {
        $data['user'] = $this->Auth_model->success_login();
        $data['title'] = 'Admin | Logo';
        $data['view'] = $this->Logo_model->view();
        // echo 'Selamat datang ' . $data['user']['nama'];
        $this->load->view('templates/header', $data);
        $this->load->view('admin/logo/index', $data);
        $this->load->view('templates/footer');
    }

    public function tambah()
    {
      $data['user'] = $this->Auth_model->success_login();
      $data['title'] = 'Admin | Tambah Logo';

      $this->form_validation->set_rules('gambar', 'gambar', [
          'required' => 'Gambar tidak boleh kosong!'
      ]);

      if  ($this->form_validation->run() == false) {
          $this->load->view('templates/header', $data);
          $this->load->view('admin/logo/tambah', $data);
          $this->load->view('templates/footer');
          // $error = array('error' => $this->upload->display_error());
      } else {
          $this->Logo_model->tambah();
          $this->session->set_flashdata('success', 'Data berhasil ditambahkan!');
          redirect('admin/logo');
      }

    }

    public function hapus($id)
    {
        $this->Logo_model->hapus($id);
        $this->session->set_flashdata('success', 'Data berhasil dihapus');
        redirect('admin/slider');
    }

    public function ubah($id = null)
    {
        $data['user'] = $this->Auth_model->success_login();
        $data['slide'] = $this->Logo_model->getById($id);
        $data['title'] = 'Admin | Ubah Logo';

        $this->form_validation->set_rules('gambar', 'gambar', [
            'required' => 'Gambar tidak boleh kosong!'
        ]);
        
        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('admin/logo/ubah', $data);
            $this->load->view('templates/footer');
        } else {
            $this->Logo_model->ubah($id);
            $this->session->set_flashdata('success', 'Data berhasil diubah!');
            redirect('admin/logo');
        }
    }
}
