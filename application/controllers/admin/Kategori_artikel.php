<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kategori_artikel extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Auth_model');
        $this->load->model('Kategori_artikel_model');
        $this->load->helper(array('url','form'));
        if (!$this->session->userdata('username')) {
                $this->session->set_flashdata('error', 'Anda belum melakukan login!');
                redirect('auth');
            }

        if ($this->session->userdata('id_akses') == 2) {
            redirect('admin/blank');
        }
    }

    public function index()
    {
        $data['user'] = $this->Auth_model->success_login();
        $data['title'] = 'Admin | Kategori_artikel';
        $data['view'] = $this->Kategori_artikel_model->view();
        // echo 'Selamat datang ' . $data['user']['nama'];
        $this->load->view('templates/header', $data);
        $this->load->view('admin/kategori_artikel/index', $data);
        $this->load->view('templates/footer');
    }

    public function tambah()
    {
      $data['user'] = $this->Auth_model->success_login();
      $data['title'] = 'Admin | Tambah Kategori';

      $this->form_validation->set_rules('nama_kategori', 'Kategori','required|trim',  [
          'required' => 'Kategori tidak boleh kosong!'
      ]);

      if  ($this->form_validation->run() == false) {
          $this->load->view('templates/header', $data);
          $this->load->view('admin/kategori_artikel/tambah', $data);
          $this->load->view('templates/footer');
          // $error = array('error' => $this->upload->display_error());
      } else {
          $this->Kategori_artikel_model->tambah();
          $this->session->set_flashdata('success', 'Data berhasil ditambahkan!');
          redirect('admin/kategori_artikel');
      }

    }

    public function hapus($id)
    {
        $this->Kategori_artikel_model->hapus($id);
        $this->session->set_flashdata('success', 'Data berhasil dihapus');
        redirect('admin/Kategori_artikel');
    }

    public function ubah($id = null)
    {
        $data['user'] = $this->Auth_model->success_login();
        $data['kategori'] = $this->Kategori_artikel_model->getById($id);
        $data['title'] = 'Admin | Ubah Kategori';

        $this->form_validation->set_rules('nama_kategori', 'Kategori','required|trim',  [
            'required' => 'Kategori tidak boleh kosong!'
        ]);

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('admin/kategori_artikel/ubah', $data);
            $this->load->view('templates/footer');
        } else {
            $this->Kategori_artikel_model->ubah($id);
            $this->session->set_flashdata('success', 'Data berhasil diubah!');
            redirect('admin/kategori_artikel');
        }
    }
}
