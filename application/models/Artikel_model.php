<?php

defined('BASEPATH') or exit('No direct script access allowed');


class Artikel_model extends CI_Model
{
  private $_table= 'artikel';
  public $id;
  public $gambar = 'default.jpg';

      public function view()
      {
        $this->db->select('*, artikel.id as id_artikel');
        $this->db->from('artikel');
        $this->db->join('kategori_artikel', 'artikel.kategori = kategori_artikel.id_kategori', 'LEFT');
        $this->db->join('user', 'artikel.id_user = user.id');
        $query = $this->db->escape($this->db->get());
        return $query->result_array();

      }
      public function NewArtikel()
      {
        $this->db->select('*, artikel.id as id_artikel');
        $this->db->from('artikel');
        $this->db->join('kategori_artikel', 'artikel.kategori = kategori_artikel.id_kategori', 'LEFT');
        $this->db->join('user', 'artikel.id_user = user.id');
        $this->db->limit(3);
        $this->db->order('tgl','ASC');
        $query = $this->db->escape($this->db->get());
        return $query->result_array();
      }
      function get_artikel_list($limit, $start)
      {
        $this->db->select('*');
        $this->db->order_by('id','DESC');
        $query = $this->db->get('artikel', $limit, $start);
        return $query;
      }
      public function viewWithLimit()
      {
        $this->db->select('*, artikel.id as id_artikel');
        $this->db->from('artikel');
        $this->db->join('kategori_artikel', 'artikel.kategori = kategori_artikel.id_kategori', 'LEFT');
        $this->db->join('user', 'artikel.id_user = user.id');
        $this->db->limit(3);
        $this->db->order_by('id_artikel','DESC');
        $query = $this->db->escape($this->db->get());
        return $query->result_array();
      }
      public function getById($id)
      {
        $data = $this->db->escape($this->db->get_where('artikel',array('id' => $id)));
        return $data->row_array();
      }
      public function getDetail($id)
      {
        $this->db->select('*, artikel.id as id_artikel');
        $this->db->from('artikel');
        $this->db->join('kategori_artikel', 'artikel.kategori = kategori_artikel.id_kategori', 'LEFT');
        $this->db->join('user', 'artikel.id_user = user.id');
        $this->db->where('artikel.id', $id);
        $query = $this->db->escape($this->db->get());
        return $query->row_array();
      }
      public function tambah()
      {
        $post = $this->input->post();
        $this->id = "";
        $this->verifikasi = $post["verifikasi"];
        $this->judul= $post["judul"];
        $this->isi = $post["isi"];
        $this->id_user = $post["id_user"];
        $this->kategori = $post["kategori"];
        $this->gambar = $this->_uploadImage();
        $this->db->insert($this->_table, $this);
      }

      private function _uploadImage()
      {
        $config['upload_path']          = './assets/images/artikel';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['file_name']            = $this->id;
        $config['overwrite']            = true;
        $config['max_size']             = 1024; // 1MB
        // $config['max_width']            = 1024;
        // $config['max_height']           = 768;

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('gambar')) {
            return $this->upload->data("file_name");
            // $gambar = $_FILES['userfile']['name'];
        }
        return "default.jpg";
    }

    public function hapus($id)
    {
        $this->_deleteImage($id);
        return $this->db->delete($this->_table, array("id" => $id));
    }

    private function _deleteImage($id)
    {
        $gambar = $this->db->get_where($this->_table,["id" => $id])->row();
        if ($gambar->gambar != "default.jpg") {
            $filename = explode(".", $gambar->gambar)[0];
            return array_map("unlink", glob(FCPATH . "assets/images/artikel/$filename.*"));
          }
    }

    public function ubah($id)
    {
      $post = $this->input->post();
      $this->id = $post["id"];
      $this->verifikasi = $post["verifikasi"];
      $this->judul= $post["judul"];
      $this->isi = $post["isi"];
      $this->kategori = $post["kategori"];
      $this->gambar = $this->_uploadImage();
      if (!empty($_FILES['gambar']['name'])) {
          $this->_deleteImage($this->id);
        $this->gambar = $this->_uploadImage();
      } else {
        $this->gambar = $post["gambar_lama"];
      }
          $this->db->update($this->_table, $this, array('id' => $post["id"]));
    }

}
