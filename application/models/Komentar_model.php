<?php

defined('BASEPATH') or exit('No direct script access allowed');


class Komentar_model extends CI_Model
{
  private $_table= 'komentar';


      public function view()
      {
        $this->db->select('*');
        $this->db->from('komentar');
        $this->db->join('artikel', 'artikel.id = komentar.id_komentar', 'LEFT');
        $query = $this->db->escape($this->db->get());
        return $query->result_array();

      }
      public function viewByArtikel($id)
      {
        $this->db->select('*');
        $this->db->from('komentar');
        $this->db->where('postingan', $id);
        $query = $this->db->escape($this->db->get());
        return $query->result_array();
      }

      public function tambah()
      {
        $post = $this->input->post();
        $this->id_komentar = "";
        $this->komentar = $post["komentar"];
        $this->email = $post["email"];
        $this->postingan = $post["postingan"];
        $this->db->insert($this->_table, $this);
      }

    public function hapus($id)
    {
        return $this->db->delete($this->_table, array("id_komentar" => $id));
    }


}
