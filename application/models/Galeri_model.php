<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Galeri_model extends CI_Model
{
  private $_table= 'galeri';
  public $id;

      public function view()
      {
        $this->db->select('*');
        $this->db->from('galeri');
        $query = $this->db->escape($this->db->get());
        return $query->result_array();
      }
      public function viewLimit()
      {
        $this->db->select('*');
        $this->db->from('galeri');
        $this->db->limit(4);
        $this->db->order_by('id','desc');
        $query = $this->db->escape($this->db->get());
        return $query->result_array();
      }
      public function getById($id)
      {
        $slide = $this->db->escape($this->db->get_where('galeri',array('id' => $id)));
        return $slide->row_array();
      }

      public function tambah()
      {
        $post = $this->input->post();
        $this->id = "";
        $this->caption = $post['caption'];
        $this->gambar = $this->_uploadImage();
        $this->db->insert($this->_table, $this);
      }

      private function _uploadImage()
      {
        $config['upload_path']          = './assets/images/galeri';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $config['file_name']            = $this->id;
        $config['overwrite']            = true;
        $config['max_size']             = 1024; // 1MB
        // $config['max_width']            = 1024;
        // $config['max_height']           = 768;

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('gambar')) {
            return $this->upload->data("file_name");
            // $gambar = $_FILES['userfile']['name'];
        }
        return "default.png";
    }

    public function hapus($id)
    {
        $this->_deleteImage($id);
        return $this->db->delete($this->_table, array("id" => $id));
    }

    private function _deleteImage($id)
    {
        $slide = $this->db->get_where($this->_table,["id" => $id])->row();
        if ($slide->gambar != "default.png") {
            $filename = explode(".", $slide->gambar)[0];
            return array_map("unlink", glob(FCPATH . "assets/images/galeri/$filename.*"));
          }
    }

    public function ubah($id)
    {
      $post = $this->input->post();
      $this->id = $post["id"];
      $this->caption = $post['caption'];
      $this->gambar = $this->_uploadImage();
      if (!empty($_FILES['gambar']['name'])) {
        $this->gambar = $this->_uploadImage();
      } else {
        $this->gambar = $post["gambar_lama"];
      }
          $this->db->update($this->_table, $this, array('id' => $post["id"]));
    }

}
