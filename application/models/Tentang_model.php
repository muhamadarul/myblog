<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Tentang_model extends CI_Model
{
  private $_table= 'tentang';
  public $id;
  public $gambar = 'default.jpg';

      public function view()
      {
        $this->db->select('*');
        $this->db->from('tentang');
        $this->db->where('id', 1);
        $query = $this->db->escape($this->db->get());
        return $query->row_array();
      }
      public function getById($id)
      {
        $data = $this->db->escape($this->db->get_where('tentang',array('id' => $id)));
        return $data->row_array();
      }
      private function _uploadImage()
      {
        $config['upload_path']          = './assets/images/tentang';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $config['file_name']            = $this->id;
        $config['overwrite']            = true;
        $config['max_size']             = 1024; // 1MB
        // $config['max_width']            = 1024;
        // $config['max_height']           = 768;

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('gambar')) {
            return $this->upload->data("file_name");
            // $gambar = $_FILES['userfile']['name'];
        }
        return "default.jpg";
    }
    private function _deleteImage($id)
    {
        $gambar = $this->db->get_where($this->_table,["id" => $id])->row();
        if ($gambar->gambar != "default.jpg") {
            $filename = explode(".", $gambar->slide)[0];
            return array_map("unlink", glob(FCPATH . "assets/images/tentang/$filename.*"));
          }
    }

    public function ubah($id)
    {
      $post = $this->input->post();
      $this->id = $post["id"];;
      $this->nama= $post["nama"];
      $this->deskripsi = $post["deskripsi"];
      $this->gambar = $this->_uploadImage();
      if (!empty($_FILES['gambar']['name'])) {
          $this->_deleteImage($this->id);
        $this->gambar = $this->_uploadImage();
      } else {
        $this->gambar = $post["gambar_lama"];
      }
          $this->db->update($this->_table, $this, array('id' => $post["id"]));
    }

}
