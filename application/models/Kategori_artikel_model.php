<?php

defined('BASEPATH') or exit('No direct script access allowed');


class Kategori_artikel_model extends CI_Model
{
  private $_table= 'kategori_artikel';


      public function view()
      {
        return $this->db->get('kategori_artikel')->result_array();

      }
      public function getById($id)
      {
        $slide = $this->db->escape($this->db->get_where('kategori_artikel',array('id_kategori' => $id)));
        return $slide->row_array();
      }

      public function tambah()
      {
        $post = $this->input->post();
        $this->id_kategori = "";
        $this->nama_kategori = $post["nama_kategori"];
        $this->db->insert($this->_table, $this);
      }

    public function hapus($id)
    {
        return $this->db->delete($this->_table, array("id_kategori" => $id));
    }

    public function ubah($id)
    {
      $post = $this->input->post();
      $this->id_kategori = $post["id"];
      $this->nama_kategori = $post["nama_kategori"];
      $this->db->update($this->_table, $this, array('id_kategori' => $post["id"]));
    }

}
