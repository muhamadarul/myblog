<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title><?= $title; ?></title>

  <!-- Custom fonts for this template-->
  <link href="<?= base_url(); ?>assets/dist/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?= base_url(); ?>assets/plugins/datatables-bs4/css/dataTables.bootstrap4.css">

  <!-- Custom styles for this page -->
  <link href="<?= base_url(); ?>assets/dist/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
  <!-- Custom styles for this template-->
  <link href="<?= base_url(); ?>assets/dist/css/sb-admin-2.min.css" rel="stylesheet">
  <!-- summernote -->
  <link rel="stylesheet" href="<?= base_url(); ?>assets/plugins/summernote/summernote-bs4.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">  <!-- JQuery - Cropper -->
  <link rel="stylesheet" href="<?= base_url(); ?>assets/plugins/cropper-master/cropper.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="<?= base_url(); ?>assets/plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="<?= base_url(); ?>assets/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
  <!-- SweetAlert2 -->
  <script src="<?= base_url(); ?>assets/plugins/sweetalert/sweetalert2.all.min.js"></script>
  <!-- summernote -->
  <link rel="stylesheet" href="<?= base_url(); ?>assets/plugins/summernote/summernote-bs4.css">

</head>
<?php $hal= $this->uri->segment(2);
?>
<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="">
        <div class="sidebar-brand-icon rotate-n-15">
          <i class="fas fa-code"></i>
        </div>
        <?php if ($user['id_akses'] == 1) : ?>
          <div class="sidebar-brand-text mx-1">Super Admin</div>
        <?php endif; ?>
        <?php if ($user['id_akses'] == 2) : ?>
          <div class="sidebar-brand-text mx-1">Sub Admin</div>
        <?php endif; ?>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item <?=($hal=="beranda")?'active':'';?>">
        <a class="nav-link" href="<?= base_url('admin/beranda'); ?>">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span></a>
      </li>

      <li class="nav-item <?= ($hal=="kategori_artikel") || ($hal=="artikel")   ? 'active' : '' ;?>">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
          <i class="far fa-fw fa-file-alt"></i>
          <span>Kelola Postingan</span></a>
          <div id="collapseThree" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
              <a class="collapse-item" href="<?= base_url('admin/artikel'); ?>">Artikel</a>
              <?php if ($user['id_akses'] == 1) : ?>
              <a class="collapse-item" href="<?= base_url('admin/kategori_artikel'); ?>">Kategori Artikel</a>
              <?php endif; ?>
            </div>
          </div>
      </li>
      <?php if ($user['id_akses'] == 1) : ?>
      <li class="nav-item <?=($hal=="komentar")?'active':'';?>">
        <a class="nav-link" href="<?= base_url('admin/komentar'); ?>">
          <i class="fas fa-fw fa-comments"></i>
          <span>Kelola Komentar</span></a>
      </li>
      <li class="nav-item <?=($hal=="galeri")?'active':'';?>">
        <a class="nav-link" href="<?= base_url('admin/galeri'); ?>">
          <i class="fas fa-fw fa-photo-video"></i>
          <span>Kelola Galeri</span></a>
      </li>
      <li class="nav-item <?= ($hal=="tentang") ? 'active' : '' ;?>">
        <a class="nav-link"  href="<?= base_url('admin/tentang'); ?>">
          <i class="fas fa-fw fa-globe"></i>
          <span>Tentang Website</span></a>
      </li>
      <li class="nav-item <?= ($hal=="manajemen_user") ? 'active' : '' ;?>">
        <a class="nav-link" href="<?= base_url('admin/manajemen_user'); ?>">
          <i class="fas fa-fw fa-user"></i>
          <span>Kelola User</span></a>
      </li>
      <?php endif; ?>

      <li class="nav-item <?= ($hal=="ubah_password") ? 'active' : '' ;?>">
        <a class="nav-link" href="<?= base_url('admin/ubah_password'); ?>">
          <i class="fas fa-fw fa-key"></i>
          <span>Ubah Password</span></a>
      </li>

      <?php if ($user['id_akses'] == 1) : ?>
      <!-- Divider -->
      <hr class="sidebar-divider my-0">
      <!-- Heading -->
      <div class="sidebar-heading">
        Front End
      </div>
      <li class="nav-item <?= ($hal=="slider") ? 'active' : '' ;?>">
        <a class="nav-link" href="<?= base_url('admin/slider'); ?>">
          <i class="fas fa-fw fa-images"></i>
          <span>Kelola Slider</span></a>
      </li>
      <li class="nav-item <?= ($hal=="logo") ? 'active' : '' ;?>">
        <a class="nav-link"  href="<?= base_url('admin/logo'); ?>">
          <i class="fas fa-fw fa-image"></i>
          <span>Kelola Logo</span></a>
      </li>
     <?php endif; ?>

      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">
            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?= $user['nama']; ?></span>
                <img class="img-profile rounded-circle" src="<?= base_url(); ?>assets/images/user/<?= $user['avatar']; ?>">
              </a>
            </li>
            <div class="topbar-divider d-none d-sm-block"></div>
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link tombol-logout" href="<?= base_url('auth/logout'); ?>">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small">Logout</span>
                <i class="fa fa-power-off"></i>
              </a>
            </li>
          </ul>

        </nav>
        <!-- End of Topbar -->
