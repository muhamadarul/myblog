<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title><?= $title; ?></title>

  <!-- Bootstrap core CSS -->
  <link href="<?= base_url(); ?>assets/dist/front/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Slider Galeri-->
  <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">

  <style>
  .carousel-item {
  height: 65vh;
  min-height: 350px;
  background: no-repeat center center scroll;
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;
  }
  .back-to-top {
      position: fixed;
      bottom: 25px;
      right: 25px;
      display: none;
  }
  </style>

</head>
<?php $hal=$this->uri->segment(1); ?>
<body>
  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark static-top">
    <div class="container">
      <a class="navbar-brand" href="<?= base_url('beranda')?>">
        <?php foreach ($logo as $row) : ?>
          <img src="<?= base_url(); ?>assets/images/logo/<?= $row['gambar']; ?>" width="100px" height="50px">
        <?php endforeach; ?>
        </a>
      <!-- <a class="navbar-brand" href="<?= base_url('beranda'); ?>">MYBLOG</a> -->
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item <?= ($hal=="beranda") ? 'active' : '' ;?>">
            <a class="nav-link" href="<?= base_url('beranda'); ?>">Beranda
              <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item <?= ($hal=="artikel") ? 'active' : '' ;?>">
            <a class="nav-link" href="<?= base_url('artikel'); ?>">Artikel Wisata</a>
          </li>
          <li class="nav-item <?= ($hal=="galeri") ? 'active' : '' ;?>">
            <a class="nav-link" href="<?= base_url('galeri'); ?>">Galeri</a>
          </li>
          <li class="nav-item <?= ($hal=="tentang") ? 'active' : '' ;?>">
            <a class="nav-link" href="<?= base_url('tentang'); ?>">Tentang</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
