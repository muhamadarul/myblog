<a id="back-to-top" href="#" class="btn btn-light btn-lg back-to-top" role="button"><i class="fas fa-chevron-up"></i></a>
<footer class="py-5 bg-dark text-white-50">
 <div class="container text-center">
   <p>Copyright &copy; Mr. A <script>document.write(new Date().getFullYear());</script></p>
 </div>
</footer>

</body>
<!-- Page Footer -->
<!-- Bootstrap core JavaScript -->
<script>
$(document).ready(function(){
	$(window).scroll(function () {
			if ($(this).scrollTop() > 50) {
				$('#back-to-top').fadeIn();
			} else {
				$('#back-to-top').fadeOut();
			}
		});
		// scroll body to 0px on click
		$('#back-to-top').click(function () {
			$('body,html').animate({
				scrollTop: 0
			}, 400);
			return false;
		});
});
</script>
<script src="<?= base_url();?>assets/dist/front/vendor/jquery/jquery.slim.min.js"></script>
<script src="<?= base_url();?>assets/dist/front/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="<?= base_url();?>assets/dist/front/vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

</html>
