<div class="success-data" data-success="<?= $this->session->flashdata('success'); ?>"></div>
<div class="warning-data" data-warning="<?= $this->session->flashdata('warning'); ?>"></div>
<div class="error-data" data-error="<?= $this->session->flashdata('error'); ?>"></div>

<div class="container-fluid">
  <!-- /.card-header -->
  <section class="content">
      <div class="card">
          <div class="card-header">
              <h3 class="card-title">Ubah Tentang</h3>
          </div>
            <div class="card-body table-responsive">
                <?php if (validation_errors()) : ?>
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <h5><i class="icon fas fa-ban"></i>Alert!</h5>
                        <?= validation_errors(); ?>
                    </div>
                <?php endif; ?>
                <?php echo form_open_multipart('admin/tentang/ubah'); ?>
                  <div class="card-body">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Judul</label>
                        <input type="text" name="nama" class="form-control" value="<?= $view['nama']; ?>">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPassword1">Deskripsi</label>
                      <textarea name="deskripsi" class="textarea" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?= $view['deskripsi']; ?></textarea>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputFile">Gambar</label>
                          <input name="gambar" type="file" class="form-control" id="exampleInputFile">
                    </div>
                    <input type="text" name="gambar_lama" value="<?= $view['gambar']; ?>" hidden>
                    <input type="text" name="id" value="<?= $view['id']; ?>" hidden>
                  <button type="submit" name="simpan" class="btn btn-primary">Simpan</button>
                </div>
          </div>
        </div>
      </section>
</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->
