<div class="success-data" data-success="<?= $this->session->flashdata('success'); ?>"></div>
<div class="warning-data" data-warning="<?= $this->session->flashdata('warning'); ?>"></div>
<div class="error-data" data-error="<?= $this->session->flashdata('error'); ?>"></div>

<div class="container-fluid">

  <!-- Page Heading -->
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Tentang Web &nbsp;<i class="fas fa-globe"></i></h1>
  </div>
        <!-- Table -->
              <div class="card shadow mb-4">
                <div class="card-header py-3">
                  <h6>Tentang Web</h6>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table table-bordered" id="example1">
                      <thead>
                        <tr>
                          <th>Nama</th>
                          <th>Deskripsi</th>
                          <th>Gambar</th>
                          <th>Aksi</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td><?= $view['nama']; ?></td>
                          <td><?= $view['deskripsi']; ?></td>
                          <td><img src="<?= base_url(); ?>assets/images/tentang/<?= $view['gambar']; ?>" alt="" width="60px" title="<?= $view['gambar']; ?>"></td>
                          <td>
                            <a href="<?= base_url('admin/tentang/ubah/'); ?><?= $view['id']; ?>" class="btn-circle btn-success btn-xs"><i class="fas fa-edit"></i></a>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->
