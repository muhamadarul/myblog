<div class="success-data" data-success="<?= $this->session->flashdata('success'); ?>"></div>
<div class="warning-data" data-warning="<?= $this->session->flashdata('warning'); ?>"></div>
<div class="error-data" data-error="<?= $this->session->flashdata('error'); ?>"></div>

<div class="container-fluid">

  <!-- Page Heading -->
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Kategori Artikel &nbsp;<i class="fas fa-articel"></i></h1>
  </div>
        <!-- Table -->
              <div class="card shadow mb-4">
                <div class="card-header py-3">
                  <h6></h6><a href="<?= base_url('admin/kategori_artikel/tambah'); ?>" class="btn btn-primary btn-sm float-right"><i class="fa fa-plus">&nbsp; Tambah Kategori</i></a>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table table-bordered" id="example1">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Nama Kategori</th>
                          <th>Aksi</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php $no = 0; foreach($view as $row) : $no++ ?>
                        <tr>
                          <td><?= $no; ?></td>
                          <td><?= $row['nama_kategori']; ?></td>
                          <td>
                            <a href="<?= base_url('admin/kategori_artikel/ubah/'); ?><?= $row['id_kategori']; ?>" class="btn-circle btn-success btn-xs"><i class="fas fa-edit"></i></a>
                            <a href="<?= base_url('admin/kategori_artikel/hapus/'); ?><?= $row['id_kategori']; ?>" class="btn-circle btn-danger btn-xs tombol-hapus"><i class="fas fa-trash"></i></a>
                          </td>
                        </tr>
                      <?php endforeach; ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->
