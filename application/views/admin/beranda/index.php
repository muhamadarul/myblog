<div class="success-data" data-success="<?= $this->session->flashdata('success'); ?>"></div>
<div class="warning-data" data-warning="<?= $this->session->flashdata('warning'); ?>"></div>
<div class="error-data" data-error="<?= $this->session->flashdata('error'); ?>"></div>

<div class="container-fluid">

  <!-- Page Heading -->
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
  </div>

  <!-- Content Row -->
  <div class="row">

    <!-- Artikel Card Example -->
    <div class="col-xl-3 col-md-4 mb-4">
      <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Artikel</div>
              <div class="h5 mb-0 font-weight-bold text-gray-800">12</div>
            </div>
            <div class="col-auto">
              <a href=""><i class="fas fa-newspaper fa-2x text-gray-300"></i></a>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Komentar Card Example -->
    <div class="col-xl-3 col-md-4 mb-4">
      <div class="card border-left-success shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Komentar</div>
              <div class="h5 mb-0 font-weight-bold text-gray-800">20</div>
            </div>
            <div class="col-auto">
              <a href=""><i class="fas fa-comments fa-2x text-gray-300"></i></a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Komentar Card Example -->
    <div class="col-xl-3 col-md-4 mb-4">
      <div class="card border-left-warning shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-success text-uppercase mb-1">User</div>
              <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $hitunguser; ?></div>
            </div>
            <div class="col-auto">
              <a href="<?= base_url('admin/manajemen_user');?>"><i class="fas fa-users fa-2x text-gray-300"></i></a>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>
<!-- /.container-fluid -->
</div>
<!-- End of Main Content -->
