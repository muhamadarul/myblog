<div class="success-data" data-success="<?= $this->session->flashdata('success'); ?>"></div>
<div class="warning-data" data-warning="<?= $this->session->flashdata('warning'); ?>"></div>
<div class="error-data" data-error="<?= $this->session->flashdata('error'); ?>"></div>

<div class="container-fluid">
  <!-- /.card-header -->
  <section class="content">
      <div class="card">
          <div class="card-header">
              <h3 class="card-title">Tambah User</h3>
          </div>
            <div class="card-body table-responsive">
                <?php if (validation_errors()) : ?>
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <h5><i class="icon fas fa-ban"></i>Alert!</h5>
                        <?= validation_errors(); ?>
                    </div>
                <?php endif; ?>
                <?php echo form_open_multipart('admin/manajemen_user/tambah'); ?>
                  <div class="card-body">
                    <div class="form-group">
                        <label>Hak Akses</label>
                        <select name="id_akses" class="form-control id_akses" style="width: 100%;">
                            <?php foreach ($akses as $row) : ?>
                                <option value="<?= $row['id']; ?>"><?= $row['level_akses']; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Nama</label>
                        <input type="text" name="nama" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Username</label>
                        <input type="text" name="username" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                    </div>
                    <div class="form-group">
                        <label>Status</label>
                        <select name="status" class="form-control select2" style="width: 100%;">
                            <option value="1" selected="selected">Aktif</option>
                            <option value="0">Tidak Aktif</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Password</label>
                        <input name="password1" type="password" class="form-control" id="exampleInputEmail1" placeholder="Masukkan Password">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Ulang Password</label>
                        <input name="password2" type="password" class="form-control" id="exampleInputEmail1" placeholder="Ulangi Password">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputFile">Avatar</label>
                          <input name="avatar" type="file" class="form-control" id="exampleInputFile">
                    </div>
                  <button type="submit" name="simpan" class="btn btn-primary">Simpan</button>
                </div>
          </div>
        </div>
      </section>
</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->
