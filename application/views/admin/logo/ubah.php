<div class="success-data" data-success="<?= $this->session->flashdata('success'); ?>"></div>
<div class="warning-data" data-warning="<?= $this->session->flashdata('warning'); ?>"></div>
<div class="error-data" data-error="<?= $this->session->flashdata('error'); ?>"></div>

<div class="container-fluid">
  <!-- /.card-header -->
  <section class="content">
      <div class="card col-lg-5">
          <div class="card-header">
              <h3 class="card-title">Ubah Logo</h3>
          </div>
            <div class="card-body table-responsive">
                <?php if (validation_errors()) : ?>
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <h5><i class="icon fas fa-ban"></i>Alert!</h5>
                        <?= validation_errors(); ?>
                    </div>
                <?php endif; ?>
                <?php echo form_open_multipart('admin/logo/ubah'); ?>
                  <div class="card-body">
                    <div class="form-group">
                        <label for="exampleInputFile">Gambar</label>
                          <input name="gambar" type="file" class="form-control" id="exampleInputFile">
                    </div>
                    <div class="form-group">
                        <input name="id" type="hidden" class="form-control" value="<?= $slide['id']; ?>">
                    </div>
                    <div class="form-group">
                        <input name="gambar_lama" type="hidden" class="form-control" id="exampleInputEmail1" placeholder="Masukkan judul" value="<?= $slide['gambar']; ?>">
                    </div>
                  <button type="SUBMIT" class="btn btn-primary">Simpan</button>
                </div>
          </div>
        </div>
      </section>
</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->
