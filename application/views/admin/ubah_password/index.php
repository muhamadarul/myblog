<div class="success-data" data-success="<?= $this->session->flashdata('success'); ?>"></div>
<div class="warning-data" data-warning="<?= $this->session->flashdata('warning'); ?>"></div>
<div class="error-data" data-error="<?= $this->session->flashdata('error'); ?>"></div>

<div class="container-fluid">

  <!-- /.card-header -->
  <section class="content">

      <div class="card">
          <div class="card-header">
              <h3 class="card-title">Ubah Password Anda</h3>
          </div>
                <div class="card-body table-responsive">
                    <?php if (validation_errors()) : ?>
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h5><i class="icon fas fa-ban"></i>Alert!</h5>
                            <?= validation_errors(); ?>
                        </div>
                    <?php endif; ?>
                    <?= $this->session->flashdata('message');?> 
                    <?php echo form_open_multipart('admin/ubah_password'); ?>
                      <div class="card-body">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Password Lama</label>
                            <input name="password_lama" type="password" class="form-control" id="exampleInputEmail1" placeholder="Masukkan Password">
                        </div>
                        <?= form_error('password_lama');?>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Password Baru</label>
                            <input name="password_baru1" type="password" class="form-control" id="exampleInputEmail1" placeholder="Masukkan Password">
                        </div>
                        <?= form_error('password_baru1');?>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Ulang Password</label>
                            <input name="password_baru2" type="password" class="form-control" id="exampleInputEmail1" placeholder="Ulangi Password">
                        </div>
                        <?= form_error('password_baru2');?>
                        <input name="id" type="hidden" value="<?= $user['id']; ?>">
                      <button type="submit" name="simpan" class="btn btn-primary">Simpan</button>
                    </div>
              </div>
        </div>
      </section>
    </div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->
