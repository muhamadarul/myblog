<div class="success-data" data-success="<?= $this->session->flashdata('success'); ?>"></div>
<div class="warning-data" data-warning="<?= $this->session->flashdata('warning'); ?>"></div>
<div class="error-data" data-error="<?= $this->session->flashdata('error'); ?>"></div>

<div class="container-fluid">
  <!-- /.card-header -->
  <section class="content">
      <div class="card">
          <div class="card-header">
              <h3 class="card-title">Tambah Artikel</h3>
          </div>
            <div class="card-body table-responsive">
                <?php if (validation_errors()) : ?>
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <h5><i class="icon fas fa-ban"></i>Alert!</h5>
                        <?= validation_errors(); ?>
                    </div>
                <?php endif; ?>
                <?php echo form_open_multipart('admin/artikel/tambah'); ?>
                  <div class="card-body">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Judul</label>
                        <input type="text" name="judul" class="form-control" placeholder="Isi Judul Artikel...">
                    </div>
                    <div class="form-group">
                        <label>Kategori Artikel</label>
                        <select name="kategori" class="form-control id_akses" style="width: 100%;">
                            <option value="">-- Pilih Kategori Artikel --</option>
                            <?php foreach ($kategori as $row) : ?>
                                <option value="<?= $row['id_kategori']; ?>"><?= $row['nama_kategori']; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group" hidden>
                        <label>Verifikasi</label>
                        <select name="verifikasi" class="form-control select2" style="width: 100%;">
                            <option value="1" selected="selected">Aktif</option>
                            <option value="0">Tidak Aktif</option>
                        </select>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPassword1">Isi</label>
                      <textarea name="isi" class="textarea" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                    </div>
                    <div class="form-group" hidden>
                      <label for="exampleInputEmail1">User</label>
                        <input type="text" name="id_user" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="<?= $user['id'];?>">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputFile">Gambar</label>
                          <input name="gambar" type="file" class="form-control" id="exampleInputFile">
                    </div>
                  <button type="submit" name="simpan" class="btn btn-primary">Simpan</button>
                </div>
          </div>
        </div>
      </section>
</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->
