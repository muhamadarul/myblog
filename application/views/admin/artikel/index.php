<div class="success-data" data-success="<?= $this->session->flashdata('success'); ?>"></div>
<div class="warning-data" data-warning="<?= $this->session->flashdata('warning'); ?>"></div>
<div class="error-data" data-error="<?= $this->session->flashdata('error'); ?>"></div>

<div class="container-fluid">

  <!-- Page Heading -->
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Kelola Artikel &nbsp;<i class="fas fa-newspaper "></i></h1>
  </div>
        <!-- Table -->
              <div class="card shadow mb-4">
                <div class="card-header py-3">
                  <h6></h6><a href="<?= base_url('admin/artikel/tambah'); ?>" class="btn btn-primary btn-sm float-right"><i class="fa fa-plus">&nbsp; Tambah Artikel</i></a>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table table-bordered" id="example1">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Judul</th>
                          <th>Author</th>
                          <!-- <th>Isi</th> -->
                          <th>Gambar</th>
                          <th>Verifikasi</th>
                          <th>Dibuat</th>
                          <th>Aksi</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php $no = 0; foreach($view as $row) : $no++ ?>
                        <tr>
                          <td><?= $no; ?></td>
                          <td><?= $row['judul']; ?></td>
                          <td><?= $row['nama']; ?></td>
                          <!-- <td><?= substr($row['isi'], 0, 50) . '......[Selengkapnya]'; ?></td> -->
                          <td><img src="<?= base_url(); ?>assets/images/artikel/<?= $row['gambar']; ?>" alt="" width="60px" title="<?= $row['gambar']; ?>"></td>
                          <td><?php if ($row['verifikasi'] == 1) : ?>
                              <span class="badge badge-success">Published</span>
                              <?php else : ?>
                              <span class="badge badge-danger">Not Published</span>
                              <?php endif; ?></td>
                          <td><p class="date"><?= date('d/m/Y', strtotime($row['dibuat'])); ?> </p></td>
                          <td>
                            <a href="<?= base_url('admin/artikel/ubah/'); ?><?= $row['id_artikel']; ?>" class="btn-circle btn-success btn-xs"><i class="fas fa-edit"></i></a>
                            <a href="<?= base_url('admin/artikel/hapus/'); ?><?= $row['id_artikel']; ?>" class="btn-circle btn-danger btn-xs tombol-hapus"><i class="fas fa-trash"></i></a>
                          </td>
                        </tr>
                      <?php endforeach; ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->
