
<!-- Page Content -->
 <div class="container">

   <div class="row">
     <!-- Post Content Column -->
     <div class="col-lg-8">

       <!-- Title -->
       <h1 class="mt-4"><?= $view['nama'];?></h1>
       <!-- Preview Image -->
       <img class="img-fluid rounded" src="<?= base_url(); ?>assets/images/tentang/<?= $view['gambar']; ?>" alt="">
       <hr>

       <!-- Post Content -->
       <p>
         <?= $view['deskripsi'];?>
       </p>

       <hr>

       <!-- Comments Form -->
     </div>
   </div>
   <!-- /.row -->

 </div>
 <!-- /.container -->
