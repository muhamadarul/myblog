<!-- Page Content -->
<div class="container">
      <h1 class="my-4 text-center text-lg-left">Thumbnail Gallery</h1>
      <div class="row text-center text-lg-left">
        <?php foreach ($galeri as $row): ?>
          <div class="col-lg-3 col-md-4 col-6">
            <a href="#" class="d-block mb-4 h-100">
                  <img class="img-fluid img-thumbnail" src="<?= base_url(); ?>assets/images/galeri/<?= $row['gambar']; ?>" width="400px" height="300px" alt="">
                  <?php echo $row['caption']; ?>
                </a>
          </div>
        <?php endforeach; ?>
      </div>
</div>
<hr>

<!-- /.container -->
