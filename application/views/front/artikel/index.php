<!-- Page Content -->
    <div class="container">

      <!-- Page Heading -->
      <h1 class="my-4">Semua Artikel Wisata
      </h1>
      <?php foreach($artikel->result_array() as $row): ?>
      <!-- Project One -->
      <div class="row">
        <div class="col-md-7">
          <a href="#">
            <img class="img-fluid rounded mb-3 mb-md-0" src="<?= base_url(); ?>assets/images/artikel/<?= $row['gambar']; ?>" width="700px" height="300px" title="<?= $row['gambar']; ?>" alt="" >
          </a>
        </div>
        <div class="col-md-5">
          <h3><?= $row['judul'];?></h3>
          <p>
            <?= $row['isi']; ?>
          </p>
          <a class="btn btn-secondary" href="<?= base_url('artikel/detail/').$row['id']; ?>">Baca Selengkapnya...</a>
        </div>
      </div>
      <!-- /.row -->
      <hr>
    <?php endforeach; ?>
      <!-- Pagination -->
      <div class="row">
          <div class="col">
              <!--Tampilkan pagination-->
              <?php echo $pagination; ?>
          </div>
      </div>

    </div>
    <!-- /.container -->
