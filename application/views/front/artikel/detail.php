
<!-- Page Content -->
 <div class="container">

   <div class="row">
     <!-- Post Content Column -->
     <div class="col-lg-8">

       <!-- Title -->
       <h1 class="mt-4"><?= $artikel['judul'];?></h1>

       <!-- Author -->
       <p class="lead">
         by
         <a href="#"><?= $artikel['nama'];?></a>
       </p>

       <hr>
       <!-- Categories -->
       <p>Category <strong> <?= $artikel['nama_kategori']; ?></strong></p>
       <!-- Date/Time -->
       <p>Posted on <?= date('d F Y H:i ', strtotime($artikel['tgl'])) ?>WIB</p>

       <!-- Preview Image -->
       <img class="img-fluid rounded" src="<?= base_url(); ?>assets/images/artikel/<?= $artikel['gambar']; ?>" alt="">

       <hr>

       <!-- Post Content -->
       <p>
         <?= $artikel['isi'];?>
       </p>

       <hr>

       <!-- Comments Form -->
       <div class="card my-4">
         <h5 class="card-header">Leave a Comment:</h5>
         <div class="card-body">
           <?php if (validation_errors()) : ?>
               <div class="alert alert-danger alert-dismissible">
                   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                   <h5><i class="icon fas fa-ban"></i>Alert!</h5>
                   <?= validation_errors(); ?>
               </div>
           <?php endif; ?>
           <?php echo form_open_multipart('artikel/saveKomentar'); ?>
             <div class="form-group">
               <label>Email</label>
               <input type="email" name="email" value="" class="form-control" required>
             </div>
             <div class="form-group">
               <label>Comment</label>
               <textarea class="form-control" name="komentar" rows="3" required></textarea>
             </div>
             <?php $postingan = $this->uri->segment(3); ?>
             <input type="text" name="postingan" value="<?php echo $postingan; ?>" hidden>
             <button type="submit" class="btn btn-primary">Submit</button>
         </div>
       </div>

       <!-- Single Comment -->
       <?php if (isset($komentar)): ?>
       <?php foreach($komentar as $komen): ?>
       <div class="media mb-4">
         <img class="d-flex mr-3 rounded-circle" src="http://placehold.it/50x50" alt="">
         <div class="media-body">
           <h5 class="mt-0"><?php echo $komen['email']; ?></h5>
           <?php echo $komen['komentar']; ?>
         </div>
       </div>
        <?php endforeach; ?>
        <?php else: ?>
          <div class="media mb-4">
            <div class="media-body">
              <h5 class="mt-0">Belum Ada Komentar</h5>
            </div>
          </div>
        <?php endif; ?>
     </div>

   </div>
   <!-- /.row -->

 </div>
 <!-- /.container -->
