<!-- Slider -->
<header>
  <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner" role="listbox">
      <!-- Slide One - Set the background image for this slide in the line below -->
      <div class="carousel-item active">
        <div class="carousel-caption d-none d-md-block">
          <h3 class="display-4">First Slide</h3>
          <p class="lead">This is a description for the first slide.</p>
        </div>
      </div>
      <div class="carousel-inner" role="listbox">
      <?php foreach($slider as $row) : ?>
      <!-- Slide Two - Set the background image for this slide in the line below -->
      <div class="carousel-item" style="background-image: url('<?= base_url(); ?>assets/images/slider/<?= $row['gambar']; ?>')">
        <div class="carousel-caption d-none d-md-block">
          <h3 class="display-4"><?= $row['judul']; ?></h3>
        </div>
      </div>
    <?php endforeach; ?>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
  </div>
</header>

              <!-- Preview Artikel -->
              <div class="container">
                <div class="row">
                  <div class="col-lg-12 text-center">
                    <h1 class="mt-5">Artikel Wisata</h1>
                  </div>
                </div>
              </div>
              <hr>
            <div class="container">
              <div class="row">
                <?php foreach ($artikel as $row): ?>
                <div class="col-md-4 mb-5">
                  <div class="card h-100">
                    <img class="card-img-top" src="<?= base_url(); ?>assets/images/artikel/<?= $row['gambar']; ?>" alt="">
                    <div class="card-body">
                      <h4 class="card-title"><?= $row['judul'];?></h4>
                      <p class="card-text"><?= $row['isi'];?></p>
                    </div>
                    <div class="card-footer">
                      <a href="<?= base_url('artikel/detail/'); ?><?= $row['id_artikel']; ?>" class="btn btn-secondary">Baca Selengkapnya....</a>
                    </div>
                  </div>
                </div>
               <?php endforeach; ?>
                <hr>
                  <div class="col-lg-12 text-center">
                    <a class="btn btn-primary" href="#">Lihat Semua Artikel Wisata</a>
                  </div>
              </div>
            </div>


        <!-- Preview Galeri -->
            <div class="container">
              <div class="row">
                <div class="col-lg-12 text-center">
                  <h1 class="mt-5">Galeri</h1>
                </div>
              </div>
            </div>
            <hr>

          <div class="container">
            <div class="row blog">
                <div class="col-md-12">
                    <div id="blogCarousel" class="carousel slide" data-ride="carousel">

                        <ol class="carousel-indicators">
                            <li data-target="#blogCarousel" data-slide-to="0" class="active"></li>
                            <li data-target="#blogCarousel" data-slide-to="1"></li>
                        </ol>

                        <!-- Carousel items -->
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <div class="row">
                                  <?php foreach ($galeri as $row): ?>
                                    <div class="col-md-3">
                                        <a href="#">
                                            <img src="<?= base_url(); ?>assets/images/galeri/<?= $row['gambar']; ?>" alt="Image" width="250px" height="250px" style="max-width:100%;">
                                            <?php echo $row['caption']; ?>
                                        </a>
                                    </div>
                                    <?php endforeach; ?>
                                </div>
                                <!--.row-->
                            </div>
                            <!--.item-->
                        </div>
                        <!--.carousel-inner-->
                    </div>
                    <!--.Carousel-->

                </div>
            </div>
          </div>
          <hr>
<!-- End Preview Galeri -->
<div class="container">

  <!-- Portfolio Item Heading -->
  <h1 class="my-4">Tentang <?= $tentang['nama'];?>
  </h1>

  <!-- Portfolio Item Row -->
  <div class="row">
    <div class="col-md-8">
      <img class="img-fluid" src="<?= base_url(); ?>assets/images/tentang/<?= $tentang['gambar']; ?>" width="750px" height="500px" alt="">
    </div>
    <div class="col-md-4">
      <h3 class="my-3"><?= $tentang['nama'];?></h3>
      <p>
        <?= substr($tentang['deskripsi'], 0, 700) . '......'; ?>
      </p>
        <div class="col-lg-12 text-center">
          <a class="btn btn-primary" href="#">Lihat Selengkapnya</a>
        </div>
    </div>
  </div>
  <!-- /.row -->
</div>

  <!-- Video Item Row -->
  <hr>
  <div class="container">

    <!-- Portfolio Item Heading -->
    <center>
    <h1 class="my-4">Ayo Berlibur</h1>
    </center>

  <div class="row">
    <div class="col-md-12">
      <center>
        <iframe frameborder="0" scrolling="no" marginheight="0" marginwidth="0"width="788.54" height="443" type="text/html" src="https://www.youtube.com/embed/Z58oCrOlH48?autoplay=0&fs=0&iv_load_policy=3&showinfo=0&rel=0&cc_load_policy=0&start=0&end=0&origin=https://youtubeembedcode.com"><div><small><a href="https://youtubeembedcode.com/en">youtubeembedcode en</a></small></div><div><small><a href="https://www.smartmenus.org/">Ultimate Web traffic</a></small></div><div><small><a href="https://youtubeembedcode.com/pl/">youtubeembedcode.com/pl/</a></small></div><div><small><a href="https://www.smartmenus.org/">Ultimate Web traffic</a></small></div></iframe>
      </center>
    </div>
  </div>
</div>
</div>
<!-- /.container -->

<hr>
